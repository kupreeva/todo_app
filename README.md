# Plan your day!
###This is a simple daily sceduler to help you keep track of all tasks and get things done. 

Technologies 
* Python 3.6
* Django 2.1
* Bootstrap
* sqlite3

What's inside?
* Welcome page
![Welcome page](screenshots/welcome.png)
* To do list
![To do list](screenshots/todolist.png)
* Registration page
![Registration page](screenshots/registration.png)
* Log in page
![Log in page](screenshots/login.png)
