from django.urls import path
from todolist import views
from django.contrib.auth.views import LoginView, LogoutView
from todolist.forms import LoginForm

urlpatterns = [
    path('', views.index, name='index'),
    path('add', views.add_todo, name='add'),
    path('complete/<todo_id>', views.complete_todo, name='complete'),
    path('delete_completed', views.delete_completed, name='delete_completed'),
    path('delete_all', views.delete_all, name='delete_all'),
    path('register', views.register, name='register'),
    path('login', LoginView.as_view(authentication_form=LoginForm), name='login'),
    path('logout', LogoutView.as_view(next_page='index'), name='logout')
]