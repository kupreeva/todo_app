from django.shortcuts import render, redirect
from todolist.models import Todo
from todolist.forms import TodoForm, CustomCreateUserForm
from django.views.decorators.http import require_POST
import datetime
from django.contrib.auth import login, authenticate


def index(request):
    """view for index.html"""
    if not request.user.is_authenticated:
        context = {}
    else:
        logged_in_user = request.user
        todo_list = Todo.objects.filter(user=logged_in_user).order_by('id')
        today = datetime.datetime.now()

        form = TodoForm()

        context = {'todo_list': todo_list, 'form': form, 'today': today}

    return render(request, 'todolist/index.html', context=context)


@require_POST
def add_todo(request):
    """view for adding task to list"""

    form = TodoForm(request.POST)
    print(request.POST['text'])

    if form.is_valid():
        new_todo = form.save(commit=False)
        new_todo.user = request.user
        new_todo.save()

    return redirect('index')


def complete_todo(request, todo_id):
    """view for completing task"""
    todo = Todo.objects.get(pk=todo_id)
    todo.complete = True
    todo.save()

    return redirect('index')


def delete_completed(request):
    """view for deleting all completed tasks"""
    logged_in_user = request.user
    Todo.objects.filter(user=logged_in_user).filter(complete__exact=True).delete()

    return redirect('index')


def delete_all(request):
    """view for deleting all tasks"""
    logged_in_user = request.user
    Todo.objects.filter(user=logged_in_user).delete()

    return redirect('index')


def register(request):
    if request.method == 'POST':
        form = CustomCreateUserForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')
    else:
        form = CustomCreateUserForm()

    context = {'form': form}
    return render(request, 'registration/register.html', context=context)