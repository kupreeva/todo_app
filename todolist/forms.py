from django import forms
from todolist.models import Todo
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class TodoForm(forms.ModelForm):
    class Meta:
        model = Todo
        fields = ['text']
        widgets = {
            'text': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'What do you need to do?'
            })}


class CustomCreateUserForm(UserCreationForm):
    username = forms.RegexField(
        label="Login",
        max_length=30,
        regex=r"^[\w.@+-]+$",
        help_text="Required. 30 characters or fewer. Letters, digits and ""@/./+/-/_ only.",
        error_messages={
            'invalid': "This value may contain only letters, numbers and ""@/./+/-/_ characters."},
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
        })
    )

    password1 = forms.CharField(
        label="Password",
        help_text="Your password must contain at least 8 characters and can't be entirely numeric",
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'required': 'true',
        })
    )

    password2 = forms.CharField(
        label="Password confirmation",
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'type': 'password',
            'required': 'true',
        }),
        help_text="Enter the same password as above, for verification."
    )


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))
    password = forms.CharField(
        label="Password",
        max_length=30,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))
